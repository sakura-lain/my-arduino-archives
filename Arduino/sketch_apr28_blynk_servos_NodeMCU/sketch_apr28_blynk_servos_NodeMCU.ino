#define BLYNK_PRINT Serial
 
#include <ESP8266WiFi.h>

#include <BlynkSimpleEsp8266.h>

#include <Servo.h> 

// You should get Auth Token in the Blynk App.

// Go to the Project Settings (nut icon).

char auth[] = "79a3201ca9a940a381ff7ceb3c014584";
 
// Your WiFi credentials.
// Set password to "" for open networks.
char ssid[] = "Bbox-E89E6B";
char pass[] = "AA551EAB4D";

//int LED = D4; // Define LED as an Integer (whole numbers) and pin D8 on Wemos D1 Mini Pro

Servo myservo; 

void setup()
{
  // Debug console
  Serial.begin(115200);
 { 
  myservo.attach(D8);  // attaches the servo on GIO2 to the servo object 
  pinMode(LED_BUILTIN, OUTPUT); //Set the LED (D8) as an output
} 

}

void loop()
{
  Blynk.run();
 
}
 
// This function will be called every time button Widget
// in Blynk app writes values to the Virtual Pin V3
BLYNK_WRITE(V3) {
 int pinValue = param.asInt(); // Assigning incoming value from pin V3 to a variable
 if (pinValue == 1) {
     int pos;

  for(pos = 0; pos <= 180; pos += 1) // goes from 0 degrees to 180 degrees 
  {                                  // in steps of 1 degree 
    myservo.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(15);                       // waits 15ms for the servo to reach the position 
  } 
  for(pos = 180; pos>=0; pos-=1)     // goes from 180 degrees to 0 degrees 
  {                                
    myservo.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(15);                       // waits 15ms for the servo to reach the position 
  } 
  digitalWrite(LED_BUILTIN, HIGH); // Turn LED on.
 }
 else {
    digitalWrite(LED_BUILTIN, LOW); // Turn LED off.
 }
}
