void setup() {
  // put your setup code here, to run once:
  pinMode(4, OUTPUT); //Sortie sur pin 4

}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(4,HIGH); //Allume 1 seconde (état haut, 1000 millisecondes)
  delay(10);
  digitalWrite(4,LOW); //Eteint 1 seconde (état bas, 1000 millisecondes)
  delay(10);
}
