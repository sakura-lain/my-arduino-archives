int resistorPin = A0;
int ledPin = 4;
int value,duration;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(ledPin,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  value = analogRead(resistorPin);
  duration = map(value,0,1023,500,50);
  Serial.println(value);
  digitalWrite(ledPin,HIGH);
  delay(duration);
  digitalWrite(ledPin,LOW);
  delay(duration);
}
