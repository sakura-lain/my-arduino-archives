    /****************************
      Réception de données via le port Série Logiciel
      Attention les câbles RX et TX doivent être croisés
      Relier les masses des 2 Arduino
    ****************************/

    #include <SoftwareSerial.h>
    SoftwareSerial mySerial(10, 11); // RX, TX
    String readString;

    void setup() {
      pinMode(LED_BUILTIN , OUTPUT);
      Serial.begin(115200);
      while (!Serial) {
        ;
      }
      Serial.println("Ecoute");
      mySerial.begin(57600);
    } // ********  setup() Fin
    void loop() {

      while (mySerial.available()) {
        delay(10); //delay to allow byte to arrive in input buffer
        char c = mySerial.read();
        readString += c;
      }

      if (readString.length() > 0) {
        Serial.println(readString);
        if (readString.indexOf("LED ON") != -1)
        {digitalWrite(LED_BUILTIN ,HIGH);}
        if (readString.indexOf("LED OFF") != -1)
        {digitalWrite(LED_BUILTIN ,LOW);}
        readString = "";
      }
    }

