    /**********************************************
       Transfert de données d'une carte arduino à une autre
       par le port série
       Les masse des deux Aduino doivent être communes
        - RX sur pin 10
        - TX sur pin 11
    ****************************************/
    #include <SoftwareSerial.h>

    SoftwareSerial mySerial(10, 11); // RX, TX

    void setup()
    {
      mySerial.begin(57600);
      Serial.begin(115200);
      pinMode(LED_BUILTIN , OUTPUT);
    }
    void loop() /* Envoi d'un ordre toutes les 2 secondes */
    {
      mySerial.write("LED ON");
      Serial.println("LED ON");
      digitalWrite(LED_BUILTIN, HIGH);
      delay(2000);
      mySerial.write("LED OFF");
      Serial.println("LED OFF");
      digitalWrite(LED_BUILTIN, LOW);
      delay(2000);
    }

