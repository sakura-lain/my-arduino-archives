int ledPin = 4;

void setup() {
  // put your setup code here, to run once:
  pinMode(ledPin, OUTPUT); //Sortie sur pin 4

}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(ledPin,HIGH); //Allume 1 seconde (état haut, 1000 millisecondes)
  delay(10);
  digitalWrite(ledPin,LOW); //Eteint 1 seconde (état bas, 1000 millisecondes)
  delay(10);
}
