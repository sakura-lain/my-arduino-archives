/*
 Fade
 
 This example shows how to fade an LED on pin 9
 using the analogWrite() function.
 
 This example code is in the public domain.
 */

int ledPin = 9;           // the pin that the LED is attached to

// the setup routine runs once when you press reset:
void setup()  { 
} 

// the loop routine runs over and over again forever:
void loop()  { 
  for(int fadeValue = 0;fadeValue <= 255;fadeValue += 5) {
  analogWrite(ledPin, fadeValue);
  delay(30);  
  }
}
