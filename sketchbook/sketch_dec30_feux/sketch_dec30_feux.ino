const int feu1Rouge = 2; //L1
const int feu1Orange = 3; //L2
const int feu1Vert= 4; //L3
const int feu2Rouge = 5; //L4
const int feu2Orange = 6; //L5
const int feu2Vert = 7; //L6

void setup()
{
  pinMode(feu1Rouge, OUTPUT);
  pinMode(feu1Orange, OUTPUT);
  pinMode(feu1Vert, OUTPUT);
  pinMode(feu2Rouge, OUTPUT);
  pinMode(feu2Orange, OUTPUT);
  pinMode(feu2Vert, OUTPUT); 
}

void loop()
{
  // LOW : allumé, HIGH : éteint
  digitalWrite(feu1Rouge, HIGH);
  digitalWrite(feu1Orange, HIGH);
  digitalWrite(feu1Vert, LOW);
  digitalWrite(feu2Rouge, LOW);
  digitalWrite(feu2Orange, HIGH);
  digitalWrite(feu2Vert, HIGH);
  delay(3000);
  
  digitalWrite(feu1Rouge, HIGH);
  digitalWrite(feu1Orange, LOW);
  digitalWrite(feu1Vert, HIGH);
  digitalWrite(feu2Rouge, LOW);
  digitalWrite(feu2Orange, HIGH);
  digitalWrite(feu2Vert, HIGH);
  delay(1000);
  
  digitalWrite(feu1Rouge, LOW);
  digitalWrite(feu1Orange, HIGH);
  digitalWrite(feu1Vert, HIGH);
  digitalWrite(feu2Rouge, LOW);
  digitalWrite(feu2Orange, HIGH);
  digitalWrite(feu2Vert, HIGH);
  delay(1000);
  
  digitalWrite(feu1Rouge, LOW);
  digitalWrite(feu1Orange, HIGH);
  digitalWrite(feu1Vert, HIGH);
  digitalWrite(feu2Rouge, HIGH);
  digitalWrite(feu2Orange, HIGH);
  digitalWrite(feu2Vert, LOW);
  delay(3000);
  
  digitalWrite(feu1Rouge, LOW);
  digitalWrite(feu1Orange, HIGH);
  digitalWrite(feu1Vert, HIGH);
  digitalWrite(feu2Rouge, HIGH);
  digitalWrite(feu2Orange, LOW);
  digitalWrite(feu2Vert, HIGH);
  delay(1000); 
  
  digitalWrite(feu1Rouge, LOW);
  digitalWrite(feu1Orange, HIGH);
  digitalWrite(feu1Vert, HIGH);
  digitalWrite(feu2Rouge, LOW);
  digitalWrite(feu2Orange, HIGH);
  digitalWrite(feu2Vert, HIGH);
  delay(1000);
}
