const int ledRouge = 2;
const int ledJaune = 3;
const int ledVerte = 4;

void setup()
{
  pinMode(ledRouge, OUTPUT);
  pinMode(ledJaune, OUTPUT);
  pinMode(ledVerte, OUTPUT);
}

void loop()
{
  digitalWrite(ledRouge, LOW);
  digitalWrite(ledJaune, HIGH);
  digitalWrite(ledVerte, HIGH);
  delay(100);
  digitalWrite(ledJaune, LOW);
  digitalWrite(ledRouge, HIGH);
  digitalWrite(ledVerte, HIGH);
  delay(100);
  digitalWrite(ledVerte, LOW);
  digitalWrite(ledRouge, HIGH);
  digitalWrite(ledJaune, HIGH);
  delay(100);
}
