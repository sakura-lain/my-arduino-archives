const int ledRouge = 2;

void setup()
{
  pinMode(ledRouge, OUTPUT);
}

void loop()
{
  digitalWrite(ledRouge, LOW);
  delay(1000);
  digitalWrite(ledRouge, HIGH);
  delay(1000);
}
