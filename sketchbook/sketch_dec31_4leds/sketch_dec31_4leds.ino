const int boutonBleu = 2;
const int boutonRouge = 3;
const int ledBlanche = 13;
const int ledRouge = 12;
const int ledJaune = 11;
const int ledVerte = 10;
int etatBoutonBleu;
int etatBoutonRouge;
int compteur = 0;

void setup()
{
  pinMode(boutonBleu, INPUT);
  pinMode(boutonRouge, INPUT);
  pinMode(ledBlanche, OUTPUT);
  pinMode(ledRouge, OUTPUT);
  pinMode(ledJaune, OUTPUT);
  pinMode(ledVerte, OUTPUT);
  etatBoutonBleu = HIGH;
  etatBoutonRouge = HIGH;
}

void loop()
{
  etatBoutonBleu = digitalRead(boutonBleu);
  etatBoutonRouge = digitalRead(boutonRouge);
  if(etatBoutonBleu == LOW)
  {
    compteur = compteur++;
  }
  if(etatBoutonRouge == LOW)
  {
    compteur = compteur--;
  }
  if(compteur <= 0)
  {
    compteur = 0;
  }
  if(compteur >= 4)
  {
    compteur = 4;
  }
  if(compteur =0)
  {
    digitalWrite(ledBlanche, HIGH);
    digitalWrite(ledRouge, HIGH);
    digitalWrite(ledJaune, HIGH);
    digitalWrite(ledVerte, HIGH);
  }
  if(compteur =1)
  {
    digitalWrite(ledBlanche, LOW);
    digitalWrite(ledRouge, HIGH);
    digitalWrite(ledJaune, HIGH);
    digitalWrite(ledVerte, HIGH);
  }
  if(compteur =2)
  {
    digitalWrite(ledBlanche, LOW);
    digitalWrite(ledRouge, LOW);
    digitalWrite(ledJaune, HIGH);
    digitalWrite(ledVerte, HIGH);
  }
  if(compteur =3)
  {
    digitalWrite(ledBlanche, LOW);
    digitalWrite(ledRouge, LOW);
    digitalWrite(ledJaune, LOW);
    digitalWrite(ledVerte, HIGH);
  }
  if(compteur =4)
  {
    digitalWrite(ledBlanche, LOW);
    digitalWrite(ledRouge, LOW);
    digitalWrite(ledJaune, LOW);
    digitalWrite(ledVerte, LOW);
  }
}
