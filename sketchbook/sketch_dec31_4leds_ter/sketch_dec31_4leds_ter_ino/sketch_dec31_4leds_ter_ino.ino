const int boutonBleu = 2;
const int boutonRouge = 3;
const int ledBlanche = 13;
const int ledRouge = 12;
const int ledJaune = 11;
const int ledVerte = 10;
int etatBouton;
int compteur = 0;
int compteurPlus = HIGH;
int compteurMoins = HIGH;

void setup()
{
  pinMode(boutonBleu, INPUT);
  pinMode(boutonRouge, INPUT);
  pinMode(ledBlanche, OUTPUT);
  pinMode(ledRouge, OUTPUT);
  pinMode(ledJaune, OUTPUT);
  pinMode(ledVerte, OUTPUT);
  etatBouton = HIGH;
}

void loop()
{
  etatBouton = digitalRead(boutonBleu);
  delay(20);
  if((etatBouton != compteurPlus) && (etatBouton == LOW))
  {
    compteur++;
  }
  compteurPlus = etatBouton;
  
  etatBouton = digitalRead(boutonRouge);
  delay(20);
  if((etatBouton != compteurMoins) && (etatBouton == LOW))
  {
    compteur--;
  }
  compteurMoins = etatBouton;
  
  if(compteur <= 0)
  {
    compteur = 0;
  }
  if(compteur >= 4)
  {
    compteur = 4;
  }
  
  affiche(compteur);
}

void affiche(int valeurRecue)
{
    digitalWrite(ledBlanche, HIGH);
    digitalWrite(ledRouge, HIGH);
    digitalWrite(ledJaune, HIGH);
    digitalWrite(ledVerte, HIGH);
  if(valeurRecue >=1)
  {
    digitalWrite(ledBlanche, LOW);
    digitalWrite(ledRouge, HIGH);
    digitalWrite(ledJaune, HIGH);
    digitalWrite(ledVerte, HIGH);
  }
  if(valeurRecue >=2)
  {
    digitalWrite(ledBlanche, LOW);
    digitalWrite(ledRouge, LOW);
    digitalWrite(ledJaune, HIGH);
    digitalWrite(ledVerte, HIGH);
  }
  if(valeurRecue >=3)
  {
    digitalWrite(ledBlanche, LOW);
    digitalWrite(ledRouge, LOW);
    digitalWrite(ledJaune, LOW);
    digitalWrite(ledVerte, HIGH);
  }
  if(valeurRecue >=4)
  {
    digitalWrite(ledBlanche, LOW);
    digitalWrite(ledRouge, LOW);
    digitalWrite(ledJaune, LOW);
    digitalWrite(ledVerte, LOW);
  }
}
