const int bouton = 2;
const int led = 13;
int etatBouton;


void setup()
{
  pinMode(bouton, INPUT);
  pinMode(led, OUTPUT);
  etatBouton = HIGH;
}

void loop()
{
  etatBouton = digitalRead(bouton);
  
  if(etatBouton == HIGH)
  {
    digitalWrite(led,HIGH);
  }
  else
  {   
    digitalWrite(led,LOW);
  }
}
