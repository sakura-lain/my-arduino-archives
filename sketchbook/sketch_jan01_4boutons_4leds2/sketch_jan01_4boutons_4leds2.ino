const int boutonBleu = 2;
const int boutonRouge = 3;
const int boutonVert = 4;
const int boutonBlanc = 5;
const int ledJaune = 10;
const int ledRouge = 11;
const int ledVerte = 12;
const int ledBlanche = 13;

int etatBoutonBleu;
int etatBoutonRouge;
int etatBoutonVert;
int etatBoutonBlanc;

void setup()
{
  pinMode(boutonBleu, INPUT);
  pinMode(boutonRouge, INPUT);
  pinMode(boutonVert, INPUT);
  pinMode(boutonBlanc, INPUT);
  pinMode(ledJaune, OUTPUT);
  pinMode(ledRouge, OUTPUT);
  pinMode(ledVerte, OUTPUT);
  pinMode(ledBlanche, OUTPUT);
  etatBoutonBleu = HIGH;
  etatBoutonRouge = HIGH;
  etatBoutonVert = HIGH;
  etatBoutonBlanc = HIGH;
}

void loop()
{
  
  etatBoutonBleu = digitalRead(boutonBleu);
  Serial.println(etatBoutonBleu);
  if(etatBoutonBleu == LOW)
  digitalWrite(ledJaune, LOW);
  if(etatBoutonBleu == HIGH)
  digitalWrite(ledJaune, HIGH);
  
  
  etatBoutonRouge = digitalRead(boutonRouge);
  if(etatBoutonRouge == LOW)
  digitalWrite(ledRouge, LOW);
  if(etatBoutonRouge == HIGH)
  digitalWrite(ledRouge, HIGH);
  
  
  etatBoutonVert = digitalRead(boutonVert);
  if(etatBoutonVert == LOW)
  digitalWrite(ledVerte, LOW);
  if(etatBoutonVert == HIGH)
  digitalWrite(ledVerte, HIGH);
  
  etatBoutonBlanc = digitalRead(boutonBlanc);
  if(etatBoutonBlanc == LOW)
  digitalWrite(ledBlanche, LOW);
  if(etatBoutonBlanc == HIGH)
  digitalWrite(ledBlanche, HIGH);
}
