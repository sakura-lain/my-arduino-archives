char lettre = 'A';
int chiffre = 0;

void setup() {
 Serial.begin(9600);
  Serial.println("------  Des chiffres et des lettres  ------"); 
}

void loop() {
 do
 {
 Serial.print(lettre);//Utiliser Serial.print plutôt que Serial.println pour un affichage horizontal
 lettre = lettre +1;
 delay(250);
 }
 while (lettre <= 90);
 
 if(lettre = 'Z')//Le if est facultatif
 { 
  Serial.println("");
  do//On peut remplacer le do...While par un While...
  {
   Serial.print(chiffre);
   chiffre = chiffre +1;
   delay(250);
  }
  while (chiffre <= 9);
 Serial.println("");//Attention à la position de retour à la ligne : si dans le do... while, renvoie chaque caractère à la ligne (=affichage vertical)
 delay(5000);
 lettre = 'A';
 chiffre = 0;
 }
}
