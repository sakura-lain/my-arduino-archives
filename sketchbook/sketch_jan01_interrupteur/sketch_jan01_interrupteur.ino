const int buttonPin = 2;    //pin pour le bouton
const int ledPin =  13;     //pin pour la lED
 
int interrupteur = LOW;       //valeur de l'interrupeteur
int toggle = 0;             //valeur pour changement d'état du bouton
 
int buttonState = 0;        //etat fugitif du bouton      
 
void setup() {
  // Initialisation de la LED en sortie
  pinMode(ledPin, OUTPUT);     
  // Initialisation du bouton en entrée
  pinMode(buttonPin, INPUT);    
}
 
void loop(){
  // Lecture de l'état du bouton
  buttonState = digitalRead(buttonPin);
 
  //On regarde si le bouton est pressé
  if (buttonState == HIGH) {    
    //on regarde si il y a un changement d'état
    if (toggle == 1) {
      // on change l'état du bouton
      interrupteur=!interrupteur;
      // on reinitialise le toggle
      toggle=0;
      // on affiche l'état de l'interupteur virtuel sur la led
      digitalWrite(ledPin, interrupteur); 
    }  
         
     
  }
  else {
    //le bouton n'est pas pressé, on active le toggle
    toggle=1;
      
  }
}
