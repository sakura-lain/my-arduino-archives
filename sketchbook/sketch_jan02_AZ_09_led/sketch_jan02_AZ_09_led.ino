const int led1 = 2;
const int led2 = 3;
const int led3 = 4;
char lettre = 'A';
int chiffre = 0;

void setup() 
{
 pinMode(led1, OUTPUT);
 pinMode(led2, OUTPUT);
 pinMode(led3, OUTPUT);
 Serial.begin(9600);
 Serial.println("------  Des chiffres et des lettres  ------"); 
 digitalWrite(led1, LOW);
 digitalWrite(led2, HIGH);
 digitalWrite(led3, HIGH);
 delay(1000);
}

void loop() 
{
  while (lettre <= 90)
 {
  Serial.print(lettre);
  lettre = lettre +1;
  digitalWrite(led2, LOW);
  digitalWrite(led1, HIGH);
  delay(250);
 }

 Serial.println("");
 
 while (chiffre <= 9)
 {
  Serial.print(chiffre);
  chiffre = chiffre +1;
  digitalWrite(led3, LOW);
  digitalWrite(led2, HIGH);
  delay(250);
 }
 
 Serial.println("");
 digitalWrite(led3, HIGH);
 delay(5000);
 lettre = 'A';
 chiffre = 0;


}


