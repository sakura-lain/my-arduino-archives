char lettre = 'A';
int chiffre = 0;

void setup() 
{
 Serial.begin(9600);
 Serial.println("------  Des chiffres et des lettres  ------"); 
}

void loop() 
{
  while (lettre <= 90)
 {
  Serial.print(lettre);//Utiliser Serial.print plutôt que Serial.println pour un affichage horizontal
  lettre = lettre +1;
  delay(250);
 }

 Serial.println("");
 
 while (chiffre <= 9)
 {
  Serial.print(chiffre);
  chiffre = chiffre +1;
  delay(250);
 }
 
 Serial.println("");
 delay(5000);
 lettre = 'A';
 chiffre = 0;

}
