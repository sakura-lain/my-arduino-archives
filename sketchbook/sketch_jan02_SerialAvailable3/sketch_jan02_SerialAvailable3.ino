void setup()
{
    Serial.begin(9600);
}

void loop()
{
    // variable contenant le caractère à lire
    char carlu = 0;
    // variable contenant le nombre de caractère disponibles dans le buffer


    while(Serial.available() > 0) // tant qu'il y a des caractères à lire
    {
        carlu = Serial.read(); // on lit le caractère
        Serial.print(carlu); // puis on le renvoi à l’expéditeur tel quel
 
    }
    // fin du programme
}
