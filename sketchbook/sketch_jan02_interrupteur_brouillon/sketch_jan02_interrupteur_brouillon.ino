const int bouton = 2;
const int led = 3;
int etatBouton;
int compteur = 0;
int compteurPlus = HIGH;
int compteurMoins = HIGH;

void setup()
{
  pinMode(bouton, INPUT);
  pinMode(led, OUTPUT);
  etatBouton = HIGH;
}

void loop()
{
  etatBouton = digitalRead(bouton);
  delay(20);
  if((etatBouton != compteurPlus) && (etatBouton == LOW))
  {
    compteur++;
  }
  compteurPlus = etatBouton;
  
  etatBouton = digitalRead(bouton);
  delay(20);
  if ((etatBouton != compteurMoins) && (etatBouton == LOW))
  {
    compteur--;
  }
  compteurMoins = etatBouton;
  
  if(compteur <= 0)
  {
    compteur = 0;
  }
  if(compteur >= 1)
  {
    compteur = 1;
  }
  
  affiche(compteur);
}
  
void affiche(int valeurRecue)
{
  digitalWrite(led, HIGH);
  
  if(valeurRecue =1)
  {
    digitalWrite(led, LOW);
  }
  
  if(valeurRecue =0)
  {
    digitalWrite(led, HIGH);
  }
}
