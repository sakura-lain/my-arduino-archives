
/*Comme expliqué ci-dessus, l’affichage de qualité se fera au travers de 3 couleurs qui seront représentées par des LEDs :

    Rouge : Danger, ne pas se baigner
    Orange : Baignade risquée pour les novices
    Vert : Tout baigne !

La zPlage est équipée de deux boutons. L’un servira à déclencher un SOS (si quelqu’un voit un nageur en difficulté par exemple). La lumière passe alors au rouge clignotant jusqu’à ce qu’un sauveteur ait appuyé sur l’autre bouton signalant "Problème réglé, tout revient à la situation précédente". Enfin, dernier point mais pas des moindres, le QG (vous) reçoit des informations météorologiques et provenant des marins au large. Ces messages sont retransmis sous forme de textos (symbolisés par la voie série) aux sauveteurs sur la plage pour qu’ils changent les couleurs en temps réel. Voici les mots-clés et leurs impacts :

    Rouge : meduse, tempete, requin : Des animaux dangereux ou la météo rendent la zPlage dangereuse. Baignade interdite
    Orange : vague : La natation est réservée aux bons nageurs
    Vert : surveillant, calme : Tout baigne, les zSauveteurs sont là et la mer est cool
    
int resultat = strcmp(motRecu, "requin");

if(resultat == 0)
Serial.print("Les chaines sont identiques");
else
Serial.print("Les chaines sont différentes");


*/


//On initialise les variables globales
int const boutonVert = 2;
int const boutonRouge = 3;
int const ledVerte = 4;
int const ledOrange = 5;
int const ledRouge = 6;
int etatBoutonVert;
int etatBoutonRouge;
char motRecu = 0;

void setup()
{
  Serial.begin(9600);
  pinMode(boutonVert, INPUT);
  pinMode(boutonRouge, INPUT);
  pinMode(ledVerte, OUTPUT);
  pinMode(ledOrange, OUTPUT);
  pinMode(ledRouge, OUTPUT);
  etatBoutonVert = HIGH;
  etatBoutonRouge = HIGH;
}

void loop()
{
}

void serialEvent()
{
  
int resultat = strcmp(motRecu, "requin");


if(Serial.available() > 0)
{
  motRecu = Serial.read();
  if(resultat == 0)
  Serial.print("Des animaux dangereux ou la météo rendent la plage dangereuse. Baignade interdite !");
}

void changerDeCouleur()
{
  
}



