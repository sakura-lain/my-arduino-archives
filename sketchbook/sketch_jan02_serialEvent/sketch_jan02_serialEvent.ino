const int maLed = 11; // on met une LED sur la broche 11

void setup()
{
    pinMode(maLed, OUTPUT); // la LED est une sortie
    digitalWrite(maLed, HIGH); // on éteint la LED
    Serial.begin(9600); // on démarre la voie série
}

void loop()
{
    delay(500); // fait une petite pause
    // on ne fait rien dans la loop
    digitalWrite(maLed, HIGH); // on éteint la LED

}

void serialEvent() // déclaration de la fonction d'interruption sur la voie série
{
    // lit toutes les données (vide le buffer de réception)
    while(Serial.read() != -1);

    // puis on allume la LED
    digitalWrite(maLed, LOW);
}

