int etat = 0;
char mot[20]; // le mot lu sur la voie série

void setup()
{
  Serial.begin(9600);
}

void loop()
{
  if(Serial.available())
    {

    // variable locale pour l'incrémentation des données du tableau
    int i = 0;

    // on lit les caractères tant qu'il y en a
    // OU si jamais le nombre de caractères lus atteint 19
    // (limite du tableau stockant le mot - 1 caractère)
    while(Serial.available() > 0 && i <= 19)
    {
        // on enregistre le caractère lu
        mot[i] = Serial.read();
        // laisse un peu de temps entre chaque accès a la mémoire
        delay(10);
        // on passe à l'indice suivant
        i++;
    }
    // on supprime le caractère '\n'
    // et on le remplace par celui de fin de chaine '\0'
    mot[i] = '\0';
    etat = comparerMot(mot);
    }
}
    
int comparerMot(char mot[])
{
    // on compare les mots "VERT" (surveillant, calme)
    if(strcmp(mot, "surveillant") == 0)
    {
        Serial.print("vert");
    }
    if(strcmp(mot, "calme") == 0)
    {
        Serial.print("vert");
    }
    // on compare les mots "ORANGE" (vague)
    if(strcmp(mot, "vague") == 0)
    {
        Serial.print("orange");
    }
    // on compare les mots "ROUGE" (meduse, tempete, requin)
    if(strcmp(mot, "meduse") == 0)
    {
        Serial.print("rouge");
    }
    if(strcmp(mot, "tempete") == 0)
    {
        Serial.print("rouge");
    }
    if(strcmp(mot, "requin") == 0)
    {
        Serial.print("rouge");
    }

    // si on a rien reconnu on renvoi ORANGE
    Serial.print("orange");

}

