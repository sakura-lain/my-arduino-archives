const int boutonSOS = 2;
const int ledRouge = 6;
int etatBoutonSOS = 0;
int interrupteur = HIGH;
int toggle = 0;

void setup()
{
  pinMode(boutonSOS, INPUT);
  pinMode(ledRouge, OUTPUT);
  digitalWrite(ledRouge, HIGH);
}

void loop()
{
  etatBoutonSOS = digitalRead(boutonSOS);
  if(etatBoutonSOS == HIGH) 
  {
    if (toggle == 1)
    {
      interrupteur=!interrupteur;
      toggle = 0;
      digitalRead(interrupteur);
      while(interrupteur == LOW)
      {
        digitalWrite(ledRouge, LOW);
        delay(500);
        digitalWrite(ledRouge, HIGH);
        delay(500);
      }
     }
  }
  else 
  {
    toggle=1;
  }
}

/*void clignotement()
{
  digitalWrite(ledRouge, LOW);
  delay(500);
  digitalWrite(ledRouge, HIGH);
  delay(500);
}*/
