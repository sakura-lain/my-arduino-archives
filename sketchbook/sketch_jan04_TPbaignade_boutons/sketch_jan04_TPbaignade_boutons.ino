const int boutonSOS = 2;
const int boutonOK = 3;
int etatBoutonSOS;
int etatBoutonOK;

void setup()
{
  pinMode(boutonSOS, INPUT);
  pinMode(boutonOK, INPUT);
  etatBoutonSOS = HIGH;
  etatBoutonOK = HIGH;
}

void loop()
{
  etatBoutonSOS = digitalRead(boutonSOS);
  etatBoutonOK = digitalRead(boutonOK);
  if(etatBoutonSOS == LOW)
  {
    digitalWrite(ledRouge, LOW);
    delay(500);
    digitalWrite(ledRouge, HIGH);
    delay(500);
  }
}
