const int boutonSOS = 2;
const int boutonOK = 3;
const int ledRouge = 6;
int etatBoutonSOS = 0;
int etatBoutonOK = 0;
int etatInterrupteur = HIGH;
int toggle = 0;

void setup()
{
  pinMode(boutonSOS, INPUT);
  pinMode(boutonOK, INPUT);
  pinMode(ledRouge, OUTPUT);
  digitalWrite(ledRouge, HIGH);
}

void loop()
{
  etatBoutonSOS = digitalRead(boutonSOS);
  etatBoutonOK = digitalRead(boutonOK);
  
  if((etatBoutonSOS == LOW) && (etatBoutonOK !=LOW))
  {
      interrupteur();
      while(etatInterrupteur == LOW)
      {
        clignotement();
      }
  }
  else if((etatBoutonOK == LOW) && (etatBoutonSOS !=LOW))
  {
    digitalWrite(ledRouge, HIGH);
    etatInterrupteur = HIGH;
    toggle = 1;
  }
  else
  {  
    toggle = 1;
  }

}

void interrupteur()
{
  if (toggle == 1)
    {
      etatInterrupteur=!etatInterrupteur;
      digitalRead(etatInterrupteur);
      toggle = 0;
    }
}


void clignotement()
{
while(interrupteur == LOW)  
  {
    digitalWrite(ledRouge, LOW);
    delay(500);
    digitalWrite(ledRouge, HIGH);
    delay(500);
  }
}
