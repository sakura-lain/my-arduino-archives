//Constantes

const int boutonSOS = 2;
const int boutonOK = 3;
const int ledRouge = 6;

//comptage
int compteur = 0;
int etatBouton;
int memSOS = HIGH;
int memOK = HIGH;

//Clignotement
long previousMillis = 0;
long interval = 1000;

//entrées/sorties
void setup()
{
  pinMode(boutonSOS, INPUT);
  pinMode(boutonOK, INPUT);
  pinMode(ledRouge, OUTPUT);
}

void loop()
{
  //Lecture bouton d'incrémentation
  etatBouton = digitalRead(boutonSOS);
  
  // Si le bouton a un état différent que celui enregistré ET que cet état est "appuyé"
  if((etatBouton != memSOS) && (etatBouton == LOW))
  {
       //Incrémentation
    compteur++;
  }
  //Enregistrement état bouton pour tour suivant
  memSOS = etatBouton;
 
  
  //Lecture bouton de décrémentation
  etatBouton = digitalRead(boutonOK);
 
   // Si le bouton a un état différent que celui enregistré ET que cet état est "appuyé"
  if((etatBouton != memOK) && (etatBouton == LOW))
  {
    //Décrémentation  
    compteur--;
  }
  //Enregistrement état bouton pour tour suivant
  memOK = etatBouton;
  
  //Limites à ne pas dépasser
  if(compteur < 0)
  {
    compteur = 0;
  }
  
  if(compteur > 1)
  {
    compteur = 1;
  }
  
clignotement(compteur);
}

void clignotement(int valeurRecue)
{
  
  if(valeurRecue >= 1)
  {
      unsigned long currentMillis = millis();  
      if(currentMillis - previousMillis > interval)
      {
        previousMillis = currentMillis;
         if (digitalRead(ledRouge) == HIGH)
            digitalWrite(ledRouge, LOW);
         else
            digitalWrite(ledRouge, HIGH);
      }
  }
  if(valeurRecue <=0)
  {
    digitalWrite(ledRouge, HIGH);
  }
}
