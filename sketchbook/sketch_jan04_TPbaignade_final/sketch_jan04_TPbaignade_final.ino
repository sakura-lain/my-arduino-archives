//Constantes

const int boutonSOS = 2;
const int boutonOK = 3;
const int ledVerte = 4;
const int ledOrange = 5;
const int ledRouge = 6;

//Voie série
char motRecu[20];
int memLedRouge = HIGH;
int memLedOrange = HIGH;
int memLedVerte = HIGH;

//comptage
int compteur = 0;
int etatBouton;
int memSOS = HIGH;
int memOK = HIGH;

//Clignotement
long previousMillis = 0;
long interval = 1000;
int etatClignotement;

//Entrées/sorties
void setup()
{
  pinMode(boutonSOS, INPUT);
  pinMode(boutonOK, INPUT);
  pinMode(ledVerte, OUTPUT);
  pinMode(ledOrange, OUTPUT);
  pinMode(ledRouge, OUTPUT);
  Serial.begin(9600);
  digitalWrite(ledRouge, HIGH);
  digitalWrite(ledOrange, HIGH);
  digitalWrite(ledVerte, HIGH);
}

void loop()
{
  etatBouton = digitalRead(boutonSOS);
  delay(10);
  if (etatBouton == LOW)
  {
    digitalWrite(ledRouge, HIGH);
    digitalWrite(ledOrange, HIGH);
    digitalWrite(ledVerte, HIGH);
    boutons();
  }

}

//Voie série
void serialEvent()
{  
  int i = 0; // Ici

  while(Serial.available() > 0 && i <=19)
  {
    motRecu[i] = {Serial.read()};
    delay(10);
    i++;
  }

  motRecu[i] = {'\0'};
  delay(10);
  	
  if((strcmp(motRecu, "requin") == 0) || (strcmp(motRecu, "meduse") == 0) || (strcmp(motRecu, "tempete") == 0))

  {
    Serial.println(motRecu);
    Serial.println("Des animaux dangereux ou la météo rendent la plage dangereuse. Baignade interdite.");
    digitalWrite(ledRouge, LOW);
    digitalWrite(ledOrange, HIGH);
    digitalWrite(ledVerte, HIGH);
    memLedRouge = digitalRead(ledRouge);
    memLedOrange = HIGH;
    memLedVerte = HIGH;
  }
  else if(strcmp(motRecu, "vague") == 0)
  {
    Serial.println(motRecu);
    Serial.println("La natation est réservée aux bons nageurs");
    digitalWrite(ledRouge, HIGH);
    digitalWrite(ledOrange, LOW);
    digitalWrite(ledVerte, HIGH);
    memLedOrange = digitalRead(ledOrange);
    memLedRouge = HIGH;
    memLedVerte = HIGH;
  }
  else if((strcmp(motRecu, "surveillant") == 0) || (strcmp(motRecu, "calme") == 0))
  {
    Serial.println(motRecu);
    Serial.println("Tout baigne, les sauveteurs sont là et la mer est cool");
    digitalWrite(ledRouge, HIGH);
    digitalWrite(ledOrange, HIGH);
    digitalWrite(ledVerte, LOW);
    memLedVerte = digitalRead(ledVerte);
    memLedRouge = HIGH;
    memLedOrange = HIGH;
  }
  else
  {
    Serial.println(motRecu);
    Serial.println("Rien à signaler.");
    digitalWrite(ledVerte, LOW);
    digitalWrite(ledVerte, memLedVerte);
    digitalWrite(ledOrange, memLedOrange);
    digitalWrite(ledRouge, memLedRouge);
  }
  motRecu[0] = '\0'; // On réinitialise à la chaine sans caractère.
}


//Boutons
void boutons()
{
  //Lecture bouton d'incrémentation
  etatBouton = digitalRead(boutonSOS);
  
  // Si le bouton a un état différent que celui enregistré ET que cet état est "appuyé"
  if((etatBouton != memSOS) && (etatBouton == LOW))
  {
       //Incrémentation
    compteur++;
  }
  //Enregistrement état bouton pour tour suivant
  memSOS = etatBouton;
 
  
  //Lecture bouton de décrémentation
  etatBouton = digitalRead(boutonOK);
 
   // Si le bouton a un état différent que celui enregistré ET que cet état est "appuyé"
  if((etatBouton != memOK) && (etatBouton == LOW))
  {
    //Décrémentation  
    compteur--;
  }
  //Enregistrement état bouton pour tour suivant
  memOK = etatBouton;
  
  //Limites à ne pas dépasser
  if(compteur < 0)
  {
    compteur = 0;
  }
  
  if(compteur > 1)
  {
    compteur = 1;
  }

//extinction de toutes les Leds
  digitalWrite(ledRouge, HIGH);
  
  if(compteur >= 1)
  {
      unsigned long currentMillis = millis();  
      if(currentMillis - previousMillis > interval)
      {
        previousMillis = currentMillis;
         if (digitalRead(ledRouge) == HIGH)
            digitalWrite(ledRouge, LOW);
         else
            digitalWrite(ledRouge, HIGH);
      }
     //Remise des leds à l'état antérieur      
       digitalWrite(ledVerte, memLedVerte);
       digitalWrite(ledOrange, memLedOrange);
       digitalWrite(ledRouge, memLedRouge);
     
  }
}

