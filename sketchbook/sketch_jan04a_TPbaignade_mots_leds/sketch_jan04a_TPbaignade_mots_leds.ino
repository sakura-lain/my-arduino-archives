const int ledVerte = 4;
const int ledOrange = 5;
const int ledRouge = 6;
char motRecu[20];

void setup()
{
  pinMode(ledVerte, OUTPUT);
  pinMode(ledOrange, OUTPUT);
  pinMode(ledRouge, OUTPUT);
  Serial.begin(9600);
  digitalWrite(ledRouge, HIGH);
  digitalWrite(ledOrange, HIGH);
  digitalWrite(ledVerte, HIGH);
}


void loop()
{
}

void serialEvent()
{
  int i = 0; // Ici

  while(Serial.available() > 0 && i <=19)
  {
    motRecu[i] = {Serial.read()};
    delay(10);
    i++;
  }

  motRecu[i] = {'\0'};
  delay(10);

  	
  if((strcmp(motRecu, "requin") == 0) || (strcmp(motRecu, "meduse") == 0) || (strcmp(motRecu, "tempete") == 0))

  {
    Serial.println(motRecu);
    Serial.println("Des animaux dangereux ou la météo rendent la plage dangereuse. Baignade interdite.");
    digitalWrite(ledRouge, LOW);
    digitalWrite(ledOrange, HIGH);
    digitalWrite(ledVerte, HIGH);
  }
  else if(strcmp(motRecu, "vague") == 0)
  {
    Serial.println(motRecu);
    Serial.println("La natation est réservée aux bons nageurs");
    digitalWrite(ledRouge, HIGH);
    digitalWrite(ledOrange, LOW);
    digitalWrite(ledVerte, HIGH);
  }
  else if((strcmp(motRecu, "surveillant") == 0) || (strcmp(motRecu, "calme") == 0))
  {
    Serial.println(motRecu);
    Serial.println("Tout baigne, les sauveteurs sont là et la mer est cool");
    digitalWrite(ledRouge, HIGH);
    digitalWrite(ledOrange, HIGH);
    digitalWrite(ledVerte, LOW);
  }
  else
  {
    Serial.println(motRecu);
    Serial.println("Rien à signaler.");
    digitalWrite(ledVerte, LOW);
  }
  motRecu[0] = '\0'; // On réinitialise à la chaine sans caractère.

}
