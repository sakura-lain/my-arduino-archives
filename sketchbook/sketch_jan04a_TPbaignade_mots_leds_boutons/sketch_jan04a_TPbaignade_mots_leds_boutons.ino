const int boutonSOS = 2;
const int boutonOK = 3;
const int ledVerte = 4;
const int ledOrange = 5;
const int ledRouge = 6;
char motRecu[20];
int etatBoutonSOS;
int etatBoutonOK;
int etatLed;

void setup()
{
  pinMode(boutonSOS, INPUT);
  pinMode(boutonOK, INPUT);
  pinMode(ledVerte, OUTPUT);
  pinMode(ledOrange, OUTPUT);
  pinMode(ledRouge, OUTPUT);
  Serial.begin(9600);
  digitalWrite(ledRouge, HIGH);
  digitalWrite(ledOrange, HIGH);
  digitalWrite(ledVerte, HIGH);
  etatBoutonSOS = HIGH;
  etatBoutonOK = HIGH;
  etatLed = HIGH;
  
}


void loop()
{
}

void serialEvent()
{
  int i = 0; // Ici

  while(Serial.available() > 0 && i <=19)
  {
    motRecu[i] = {Serial.read()};
    delay(10);
    i++;
  }

  motRecu[i] = {'\0'};
  delay(10);

  	
  if((strcmp(motRecu, "requin") == 0) || (strcmp(motRecu, "meduse") == 0) || (strcmp(motRecu, "tempete") == 0))

  {
    Serial.println(motRecu);
    Serial.println("Des animaux dangereux ou la météo rendent la plage dangereuse. Baignade interdite.");
    digitalWrite(ledRouge, LOW);
    digitalWrite(ledOrange, HIGH);
    digitalWrite(ledVerte, HIGH);
    etatLed = digitalRead(ledRouge);
  }
  else if(strcmp(motRecu, "vague") == 0)
  {
    Serial.println(motRecu);
    Serial.println("La natation est réservée aux bons nageurs");
    digitalWrite(ledRouge, HIGH);
    digitalWrite(ledOrange, LOW);
    digitalWrite(ledVerte, HIGH);
    etatLed = digitalRead(ledOrange);
  }
  else if((strcmp(motRecu, "surveillant") == 0) || (strcmp(motRecu, "calme") == 0))
  {
    Serial.println(motRecu);
    Serial.println("Tout baigne, les sauveteurs sont là et la mer est cool");
    digitalWrite(ledRouge, HIGH);
    digitalWrite(ledOrange, HIGH);
    digitalWrite(ledVerte, LOW);
    etatLed = digitalRead(ledVerte);
  }
  else
  {
    Serial.println(motRecu);
    Serial.println("Rien à signaler.");
    digitalWrite(ledRouge, HIGH);
    digitalWrite(ledOrange, HIGH);
    digitalWrite(ledVerte, LOW);
    etatLed = digitalRead(ledVerte);
  }
  motRecu[0] = '\0'; // On réinitialise à la chaine sans caractère.
}

void noyade()
{
  etatBoutonSOS = digitalRead(boutonSOS);
  etatBoutonOK = digitalRead(boutonOK);
  if(etatBoutonSOS == LOW)
  {
    digitalWrite(ledRouge, LOW);
    delay(500);
    digitalWrite(ledRouge, HIGH);
    delay(500);
  }
}
