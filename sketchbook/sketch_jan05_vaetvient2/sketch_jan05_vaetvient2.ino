const int boutons[4] = {2,3,4,5};
const int led = 13;

int compteur = 0;

void setup()
{
  pinMode(led, OUTPUT);
  digitalWrite(led, HIGH);

  pinMode(boutons[4], INPUT);
  digitalWrite(boutons[4], HIGH);
}

void loop()

{
//compteur
  
  if(compteur == 1)
  {
    digitalWrite(led, LOW);
  }
  else
  {
    digitalWrite(led, HIGH);
  }
  
  if(compteur >1)
  {
    compteur = 1;
  }
  if(compteur <0)
  {
    compteur = 0;
  }
  
//changement d'état du compteur et de la led
  
  
  if((digitalRead(boutons[1]) == LOW) || (digitalRead(boutons[2]) == LOW) || (digitalRead(boutons[3]) == LOW) || (digitalRead(boutons[4]) == LOW))  
  {
    compteur =! compteur;
  }
}
