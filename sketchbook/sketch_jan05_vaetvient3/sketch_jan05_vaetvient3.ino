const int boutons[4] = {2,3,4,5};
const int led = 13;

void setup()
{
  
  pinMode(led, OUTPUT);
  digitalWrite(led, HIGH);
  for(int i=0; i<4; i++)
  {
    pinMode(boutons[i], INPUT);
    digitalWrite(boutons[i], HIGH);
  }
}

void loop()

{
  int etatLed = digitalRead(led);
  
  if((digitalRead(boutons[1]) == LOW) || (digitalRead(boutons[2]) == LOW) || (digitalRead(boutons[3]) == LOW) || (digitalRead(boutons[4]) == LOW) && (etatLed == HIGH)) 
  {
    digitalWrite(led, !etatLed);
  }
  if((digitalRead(boutons[1]) == LOW) || (digitalRead(boutons[2]) == LOW) || (digitalRead(boutons[3]) == LOW) || (digitalRead(boutons[4]) == LOW) && (etatLed == LOW))  
  {
    digitalWrite(led, !etatLed);
  }

}
