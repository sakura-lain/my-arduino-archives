const int bouton1 = 2;
const int bouton2 = 3;
const int bouton3 = 4;
const int bouton4 = 5;
const int led = 13;
int compteur = 0;

void setup()
{
  
  pinMode(led, OUTPUT);
  digitalWrite(led, HIGH);
  pinMode(bouton1, INPUT);
  pinMode(bouton2, INPUT);
  pinMode(bouton3, INPUT);
  pinMode(bouton4, INPUT);
}

void loop()

{
  if(compteur == 1)
  {
    digitalWrite(led, LOW);
  }
  else
  {
    digitalWrite(led, HIGH);
  }
  
  if(compteur >1)
  {
    compteur = 1;
  }
  if(compteur <0)
  {
    compteur = 0;
  }
  
//changement d'état du compteur et de la led
  
  
   if((digitalRead(bouton1) == LOW) || (digitalRead(bouton2) == LOW) || (digitalRead(bouton3) == LOW) || (digitalRead(bouton4) == LOW))  
  {
    compteur = !compteur;
  }

}
