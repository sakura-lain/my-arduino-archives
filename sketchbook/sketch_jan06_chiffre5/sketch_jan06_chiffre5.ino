const int ledA = 2;
const int ledB = 3;
const int ledC = 4;
const int ledD = 5;
const int ledE = 6;
const int ledF = 7;
const int ledG = 8;

void setup()
{ 
  pinMode(ledA, OUTPUT);
  pinMode(ledB, OUTPUT);
  pinMode(ledC, OUTPUT);
  pinMode(ledD, OUTPUT);
  pinMode(ledE, OUTPUT);
  pinMode(ledF, OUTPUT);
  pinMode(ledG, OUTPUT);
  
  digitalWrite(ledA, HIGH);
  digitalWrite(ledB, HIGH);
  digitalWrite(ledC, HIGH);
  digitalWrite(ledD, HIGH);
  digitalWrite(ledE, HIGH);
  digitalWrite(ledF, HIGH);
  digitalWrite(ledG, HIGH);
}

void loop()
{
    //afficher 5 :
  digitalWrite(ledB, LOW);
  digitalWrite(ledE, LOW);
}
