float volt; //le voltage du capteur
float temp; // On initialise la variable de température
void setup()
{
  Serial.begin(9600);
}
void loop()
{
   volt = analogRead(0); //On lit les données du capteur sur le port ou tu l'as branché (dans cet exemple c'est le port analogique 0)
   volt = 5*volt/1023; //on calcule le voltage
   temp = volt/0.01; //puis la température
   Serial.println(temp); //on affiche la valeur (ici dans ton IDE)
   delay(1000); //un délai pour que le tout soit lisible
}
