



void setup() 
{
  Serial.begin(9600);
  for (int thisPin = 2; thisPin < 8; thisPin++) {
    pinMode(thisPin, OUTPUT);
  }
}

void loop() 
{
  if (Serial.available() > 0) {
    int inByte = Serial.read();

    switch (inByte) 
    {
      case '0':// = 48 dans la table ASCII
        digitalWrite(2, HIGH);
        digitalWrite(3, HIGH);
        digitalWrite(4, HIGH);
        digitalWrite(5, HIGH);
        digitalWrite(6, HIGH);
        digitalWrite(7, HIGH);
        digitalWrite(8, LOW);
        delay(2000);
        break;

      case '1'://
        digitalWrite(2, LOW);
        digitalWrite(3, HIGH);
        digitalWrite(4, HIGH);
        digitalWrite(5, LOW);
        digitalWrite(6, LOW);
        digitalWrite(7, LOW);
        digitalWrite(8, LOW);
        break;

     case '3':
         digitalWrite(2, HIGH);
         digitalWrite(3, HIGH);
         digitalWrite(4, HIGH);
         digitalWrite(5, HIGH);
         digitalWrite(6, LOW);
         digitalWrite(7, LOW);
         digitalWrite(8, HIGH);
         delay(2000);
         break;

      case '4':
          digitalWrite(2, LOW);
          digitalWrite(3, HIGH);
          digitalWrite(4, HIGH);
          digitalWrite(5, LOW);
          digitalWrite(6, LOW);
          digitalWrite(7, HIGH);
          digitalWrite(8, HIGH);
          break;
 
      case '5':
          digitalWrite(2, HIGH);
          digitalWrite(3, HIGH);
          digitalWrite(4, HIGH);
          digitalWrite(5, LOW);
          digitalWrite(6, LOW);
          digitalWrite(7, HIGH);
          digitalWrite(8, HIGH);
          break;

      default:
               // turn all the LEDs off:
        for (int thisPin = 2; thisPin < 8; thisPin++) {
          digitalWrite(thisPin, LOW);

                 /* case 6:
                  digitalWrite(leds[0], HIGH);
                  digitalWrite(leds[1], LOW);
                  digitalWrite(leds[2], HIGH);
                  digitalWrite(leds[3], HIGH);
                  digitalWrite(leds[4], HIGH);
                  digitalWrite(leds[5], HIGH);
                  digitalWrite(leds[6], HIGH);
                  break;

                  case 7:
                  digitalWrite(leds[0], HIGH);
                  digitalWrite(leds[1], HIGH);
                  digitalWrite(leds[2], HIGH);
                  digitalWrite(leds[3], LOW);
                  digitalWrite(leds[4], LOW);
                  digitalWrite(leds[5], LOW);
                  digitalWrite(leds[6], LOW);
                  break;

                  case 8:
                  digitalWrite(leds[0], HIGH);
                  digitalWrite(leds[1], HIGH);
                  digitalWrite(leds[2], HIGH);
                  digitalWrite(leds[3], HIGH);
                  digitalWrite(leds[4], HIGH);
                  digitalWrite(leds[5], HIGH);
                  digitalWrite(leds[6], HIGH);
                  break;

                  case 9:
                  digitalWrite(leds[0], HIGH);
                  digitalWrite(leds[1], HIGH);
                  digitalWrite(leds[2], HIGH);
                  digitalWrite(leds[3], HIGH);
                  digitalWrite(leds[4], LOW);
                  digitalWrite(leds[5], HIGH);
                  digitalWrite(leds[6], HIGH);
                  break;*/
        }
        delay(1000);
    }
  }
}
