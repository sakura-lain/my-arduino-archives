const int leds[7] = {2,3,4,5,6,7,8};
int chiffre =0;


void setup()
{

    Serial.begin(9600);
    Serial.println("------  Des chiffres et des chiffres  ------"); 
    //les sorties (7 LED) éteintes
    for(int i=0; i<7; i++)
    {
        pinMode(leds[i], OUTPUT);
        digitalWrite(leds[i], LOW);
    }
}

void loop()
{    

for (int i = 0; i < 7; i++) // on parcourt le tableau

  {

 digitalWrite(leds[0], HIGH);
 digitalWrite(leds[1], HIGH);
 digitalWrite(leds[2], HIGH);
 digitalWrite(leds[3], HIGH);
 digitalWrite(leds[4], HIGH);
 digitalWrite(leds[5], HIGH);
 digitalWrite(leds[6], LOW);
 Serial.print("0");
 delay(2000);
 

 digitalWrite(leds[0], LOW);
 digitalWrite(leds[1], HIGH);
 digitalWrite(leds[2], HIGH);
 digitalWrite(leds[3], LOW);
 digitalWrite(leds[4], LOW);
 digitalWrite(leds[5], LOW);
 digitalWrite(leds[6], LOW);
 Serial.print("1");
 delay(2000);

 digitalWrite(leds[0], HIGH);
 digitalWrite(leds[1], HIGH);
 digitalWrite(leds[2], LOW);
 digitalWrite(leds[3], HIGH);
 digitalWrite(leds[4], HIGH);
 digitalWrite(leds[5], LOW);
 digitalWrite(leds[6], HIGH);
 Serial.print("2");
 delay(2000);

 digitalWrite(leds[0], HIGH);
 digitalWrite(leds[1], HIGH);
 digitalWrite(leds[2], HIGH);
 digitalWrite(leds[3], HIGH);
 digitalWrite(leds[4], LOW);
 digitalWrite(leds[5], LOW);
 digitalWrite(leds[6], HIGH);
 Serial.print("3");
 delay(2000);
 
 digitalWrite(leds[0], LOW);
 digitalWrite(leds[1], HIGH);
 digitalWrite(leds[2], HIGH);
 digitalWrite(leds[3], LOW);
 digitalWrite(leds[4], LOW);
 digitalWrite(leds[5], HIGH);
 digitalWrite(leds[6], HIGH);
 Serial.print("4");
 delay(2000);
 
 digitalWrite(leds[0], HIGH);
 digitalWrite(leds[1], LOW);
 digitalWrite(leds[2], HIGH);
 digitalWrite(leds[3], HIGH);
 digitalWrite(leds[4], LOW);
 digitalWrite(leds[5], HIGH);
 digitalWrite(leds[6], HIGH);
 Serial.print("5");
 delay(2000);

 digitalWrite(leds[0], HIGH);
 digitalWrite(leds[1], LOW);
 digitalWrite(leds[2], HIGH);
 digitalWrite(leds[3], HIGH);
 digitalWrite(leds[4], HIGH);
 digitalWrite(leds[5], HIGH);
 digitalWrite(leds[6], HIGH);
 Serial.print("6");
 delay(2000);

 digitalWrite(leds[0], HIGH);
 digitalWrite(leds[1], HIGH);
 digitalWrite(leds[2], HIGH);
 digitalWrite(leds[3], LOW);
 digitalWrite(leds[4], LOW);
 digitalWrite(leds[5], LOW);
 digitalWrite(leds[6], LOW);
 Serial.print("7");
 delay(2000);

 digitalWrite(leds[0], HIGH);
 digitalWrite(leds[1], HIGH);
 digitalWrite(leds[2], HIGH);
 digitalWrite(leds[3], HIGH);
 digitalWrite(leds[4], HIGH);
 digitalWrite(leds[5], HIGH);
 digitalWrite(leds[6], HIGH);
 Serial.print("8");
 delay(2000);

 digitalWrite(leds[0], HIGH);
 digitalWrite(leds[1], HIGH);
 digitalWrite(leds[2], HIGH);
 digitalWrite(leds[3], HIGH);
 digitalWrite(leds[4], LOW);
 digitalWrite(leds[5], HIGH);
 digitalWrite(leds[6], HIGH);
 Serial.print("9");
 delay(2000);
  

  }

}
