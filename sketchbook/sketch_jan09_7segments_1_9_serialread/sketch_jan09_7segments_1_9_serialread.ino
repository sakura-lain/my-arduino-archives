const int leds[7] = {2,3,4,5,6,7,8};
int chiffre =0;


void setup()
{

    Serial.begin(9600);
    //les sorties (7 LED) éteintes
    for(int i=0; i<7; i++)
    {
        pinMode(leds[i], OUTPUT);
        digitalWrite(leds[i], LOW);
    }
}

void loop()
{    
}

void serialEvent()
{
  
while(Serial.available() > 0)
{
  chiffre = Serial.read();

for (int i = 0; i < 7; i++) // on parcourt le tableau

  {
    switch(chiffre)
    {

case '0':
 digitalWrite(leds[0], HIGH);
 digitalWrite(leds[1], HIGH);
 digitalWrite(leds[2], HIGH);
 digitalWrite(leds[3], HIGH);
 digitalWrite(leds[4], HIGH);
 digitalWrite(leds[5], HIGH);
 digitalWrite(leds[6], LOW);
 break;
 
case '1':
 digitalWrite(leds[0], LOW);
 digitalWrite(leds[1], HIGH);
 digitalWrite(leds[2], HIGH);
 digitalWrite(leds[3], LOW);
 digitalWrite(leds[4], LOW);
 digitalWrite(leds[5], LOW);
 digitalWrite(leds[6], LOW);
 break;
 
 
case '2':
 digitalWrite(leds[0], HIGH);
 digitalWrite(leds[1], HIGH);
 digitalWrite(leds[2], LOW);
 digitalWrite(leds[3], HIGH);
 digitalWrite(leds[4], HIGH);
 digitalWrite(leds[5], LOW);
 digitalWrite(leds[6], HIGH);
 break;

case '3':
 digitalWrite(leds[0], HIGH);
 digitalWrite(leds[1], HIGH);
 digitalWrite(leds[2], HIGH);
 digitalWrite(leds[3], HIGH);
 digitalWrite(leds[4], LOW);
 digitalWrite(leds[5], LOW);
 digitalWrite(leds[6], HIGH);
 break;
 
 case '4': 
 digitalWrite(leds[0], LOW);
 digitalWrite(leds[1], HIGH);
 digitalWrite(leds[2], HIGH);
 digitalWrite(leds[3], LOW);
 digitalWrite(leds[4], LOW);
 digitalWrite(leds[5], HIGH);
 digitalWrite(leds[6], HIGH);
 break;
 
 case '5': 
 digitalWrite(leds[0], HIGH);
 digitalWrite(leds[1], LOW);
 digitalWrite(leds[2], HIGH);
 digitalWrite(leds[3], HIGH);
 digitalWrite(leds[4], LOW);
 digitalWrite(leds[5], HIGH);
 digitalWrite(leds[6], HIGH);
 break;
 
 case '6':
 digitalWrite(leds[0], HIGH);
 digitalWrite(leds[1], LOW);
 digitalWrite(leds[2], HIGH);
 digitalWrite(leds[3], HIGH);
 digitalWrite(leds[4], HIGH);
 digitalWrite(leds[5], HIGH);
 digitalWrite(leds[6], HIGH);
 break;

case '7':
 digitalWrite(leds[0], HIGH);
 digitalWrite(leds[1], HIGH);
 digitalWrite(leds[2], HIGH);
 digitalWrite(leds[3], LOW);
 digitalWrite(leds[4], LOW);
 digitalWrite(leds[5], LOW);
 digitalWrite(leds[6], LOW);
 break;

case '8':
 digitalWrite(leds[0], HIGH);
 digitalWrite(leds[1], HIGH);
 digitalWrite(leds[2], HIGH);
 digitalWrite(leds[3], HIGH);
 digitalWrite(leds[4], HIGH);
 digitalWrite(leds[5], HIGH);
 digitalWrite(leds[6], HIGH);
 break;
 

case '9':
 digitalWrite(leds[0], HIGH);
 digitalWrite(leds[1], HIGH);
 digitalWrite(leds[2], HIGH);
 digitalWrite(leds[3], HIGH);
 digitalWrite(leds[4], LOW);
 digitalWrite(leds[5], HIGH);
 digitalWrite(leds[6], HIGH);
 break;
 
 default:
 digitalWrite(leds[0], LOW);
 digitalWrite(leds[1], LOW);
 digitalWrite(leds[2], LOW);
 digitalWrite(leds[3], LOW);
 digitalWrite(leds[4], LOW);
 digitalWrite(leds[5], LOW);
 digitalWrite(leds[6], LOW);
 break;
 
    }
  }
  }

}
