const int verrou = 11;
const int donnee = 10;
const int horloge = 12;
char sens = MSBFIRST;

void setup()
{
  pinMode(verrou, OUTPUT);
  pinMode(donnee, OUTPUT);
  pinMode(horloge, OUTPUT);
}

void loop()
{
  for(int i=0; i<9; i=i++)
  
  {
    i = pow(float(2), float(i)) - 1;
    digitalWrite(verrou, LOW);
    shiftOut(donnee, horloge, sens, ~(0x01 << i));
    digitalWrite(verrou, HIGH);
    delay(100);
  }
  sens = !sens;
}
