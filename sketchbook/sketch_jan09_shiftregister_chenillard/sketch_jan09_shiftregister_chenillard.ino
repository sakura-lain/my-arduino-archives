/* On compte de 1 à 128 en base deux
Rappel : 1 = 00000001
2 = 00000010
4 = 00000100
etc.
128 = 10000000
On affiche les valeurs binaires de 1, 2, 4, 8, 16, 32, 64, 128
*/

//Broche connectée au ST_CP du 74HC595
const int verrou = 11;
//Broche connectée au SH_CP du 74HC595
const int horloge = 12;
//Broche connectée au DS du 74HC595
const int data = 10;
 
void setup()
{
    //On met les broches en sortie
    pinMode(verrou, OUTPUT);
    pinMode(horloge, OUTPUT);
    pinMode(data, OUTPUT);
}
 
void loop()
{
    //on parcourt le tableau de 1 à 128 :
    for (int i = 1; i < 129; i=(i*2))
    {
        //On active le verrou le temps de transférer les données
        digitalWrite(verrou, LOW);
        //on envoi toutes les données grâce à shiftOut (octet inversée avec '~' pour piloter les LED à l'état bas)
        shiftOut(data, horloge, LSBFIRST, ~i);
        //et enfin on relache le verrou
        digitalWrite(verrou, HIGH);
        //une petite pause pour constater l'affichage
        delay(100);
    }
    //on parcourt le tableau de 128 à 1 :
    for (int i = 128; i >0; i=(i/2))
    {
        //On active le verrou le temps de transférer les données
        digitalWrite(verrou, LOW);
        //on envoi toutes les données grâce à shiftOut (octet inversée avec '~' pour piloter les LED à l'état bas)
        shiftOut(data, horloge, LSBFIRST, ~i);
        //et enfin on relache le verrou
        digitalWrite(verrou, HIGH);
        //une petite pause pour constater l'affichage
        delay(100);
    }
}
