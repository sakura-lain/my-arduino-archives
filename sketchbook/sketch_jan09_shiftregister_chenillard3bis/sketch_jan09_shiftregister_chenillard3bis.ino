const int verrou = 11;
const int donnee = 10;
const int horloge = 12;
char sens = MSBFIRST;

void setup()
{
  pinMode(verrou, OUTPUT);
  pinMode(donnee, OUTPUT);
  pinMode(horloge, OUTPUT);
}

void loop()
{
  for(int i=16; i<129; i=(i*2))
  {
    for(int j=0; j<4; j++)
    {
       for(int k=24; k<230; k=((i*2) + pow(float(2), float(j))))
       {
        digitalWrite(verrou, LOW);
        shiftOut(donnee, horloge, sens, ~(0x01 << i));
        digitalWrite(verrou, HIGH);
        delay(500);
       }
    }
  }
  sens = !sens;
}
