//Broche connectée au ST_CP du 74HC595
const int verrou = 11;
//Broche connectée au SH_CP du 74HC595
const int horloge = 12;
//Broche connectée au DS du 74HC595
const int data = 10;
char sens = MSBFIRST;
//on commence à aller de droite vers gauche
 
void setup()
{
    //On met les broches en sortie
    pinMode(verrou, OUTPUT);
    pinMode(horloge, OUTPUT);
    pinMode(data, OUTPUT);
}

void loop()
{
    //on ne fait la boucle que 7 fois pour ne pas se répéter au début et à la fin
    for (int i = 0; i < 7; i++)
    {
        //On active le verrou le temps de transférer les données
        digitalWrite(verrou, LOW);
        //on envoie la donnée
        //On va décaler l'octet 00000001 i fois puis l'envoyer
        shiftOut(data, horloge, sens, ~(0x01 << i));
        //et enfin on relache le verrou
        digitalWrite(verrou, HIGH);
        //une petite pause pour constater l'affichage
        delay(250);
    }
    //on inverse le sens d'affichage pour la prochaine fois (MSBFIRST  LSBFIRST)
    sens = !sens;
}
