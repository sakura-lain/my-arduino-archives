void setup()
{
  Serial.begin(9600);
}

void loop()
{
    for(int j=0; j<10; j++)
    {
      int nombre = (1<<j);//On utilise une opération par décalage de bit pour la puissance de 2, plus précise ici que la fonction pow()
      Serial.println(nombre);
      delay(1000);
    }
}
