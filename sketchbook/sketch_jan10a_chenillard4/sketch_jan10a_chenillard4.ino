const int verrou = 11;
const int donnee = 10;
const int horloge = 12;
char sens = MSBFIRST;

void setup()
{
  pinMode(verrou, OUTPUT);
  pinMode(donnee, OUTPUT);
  pinMode(horloge, OUTPUT);
}

void loop()
{

  for(int i=128; i>15; i=(i/2)) //128, 64, 32, 16
  {
    for(int j=0; j<4; j++)//0, 1, 2, 
    {
        for(int k =129; k >23; k = (i+(1<<j)))//Puissance de 2 obtenue par décalage de bit
        //if((nombre == 129) || (nombre == 66) || (nombre == 36) || (nombre == 24))
        //{
          //for(int k= 24; k< 130; k = nombre)
          {
            digitalWrite(verrou, LOW);
            shiftOut(donnee, horloge, sens, ~(0x01 << k));
            digitalWrite(verrou, HIGH);
            delay(100);
          }
        //}
    }
  }
  sens = !sens;
}


