const int verrou = 11;
const int donnee = 10;
const int horloge = 12;

char premier = 0x3f; //en binaire : 00001000
char second = 0x77; //en binaire : 00100011

void setup()
{
    //on déclare les broches en sortie
    pinMode(verrou, OUTPUT);
    pinMode(donnee, OUTPUT);
    pinMode(horloge, OUTPUT);
    //puis on envoie les données juste une fois
    //on commence par mettre le verrou
    digitalWrite(verrou, LOW);
    //on envoie la seconde donnée d'abord
    //les LEDs vertes du montage
    shiftOut(donnee, horloge, LSBFIRST, second);
    //on envoie la première donnée
    //Les LEDs rouges du montage
    shiftOut(donnee, horloge, LSBFIRST, premier);
    //et on relache le verrou pour mettre à jour les données
    digitalWrite(verrou, HIGH);
}

void loop()
{
    //rien à faire
}
