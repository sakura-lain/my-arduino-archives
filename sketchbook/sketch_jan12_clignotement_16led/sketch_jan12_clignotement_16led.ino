const int verrou = 11;
const int donnee = 10;
const int horloge = 12;
char sens = LSBFIRST;

void setup()
{
    //on déclare les broches en sortie
    pinMode(verrou, OUTPUT);
    pinMode(donnee, OUTPUT);
    pinMode(horloge, OUTPUT);
 
}

void loop()
{
    for(int i=0; i<16; i++)
    {               
        //on commence par mettre le verrou         
        digitalWrite(verrou, LOW);         
        //on envoie la seconde donnée d'abord         
        //On envoie les 8 premiers bits         
        shiftOut(donnee, horloge, sens, ~(0x00AA));         
        //on envoie la première donnée         
        //On envoie les 8 derniers bits         
        shiftOut(donnee, horloge, sens, ~(0xAA00) >> 8);
        //et on relache le verrou pour mettre à jour les données
        digitalWrite(verrou, HIGH);
        delay(10);
       
    }
    sens = !sens;
}
