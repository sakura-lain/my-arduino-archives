const int verrou = 11;
const int donnee = 10;
const int horloge = 12;

const int hexa[16] = {0x3f, 0x06, 0x5b, 0x4f, 0x66, 0x6d, 0x7d, 0x07, 0x7f, 0x6f, 0x77, 0x7c, 0x39, 0x5e, 0x79, 0x71};
//const char
//const byte
int chiffre;

void setup()
{
    pinMode(verrou, OUTPUT);
    pinMode(donnee, OUTPUT);
    pinMode(horloge, OUTPUT);     
}

void loop()
{   
    for(int i=0; i<17; i++)
    {
        chiffre = hexa[i];  
        while(chiffre <17)
        {
            chiffre ++;
        }
        digitalWrite(verrou, LOW);
        shiftOut(donnee, horloge, LSBFIRST, chiffre);
        shiftOut(donnee, horloge, LSBFIRST, chiffre);
        digitalWrite(verrou, HIGH); 
        delay(500); 
    }
}
