const int verrou = 11;
const int donnee = 10;
const int horloge = 12;

const int hexa[16] = {0x3f, 0x06, 0x5b, 0x4f, 0x66, 0x6d, 0x7d, 0x07, 0x7f, 0x6f, 0x77, 0x7c, 0x39, 0x5e, 0x79, 0x71};
//const char
//const byte
int unite;
int seizaine;

void setup()
{
    pinMode(verrou, OUTPUT);
    pinMode(donnee, OUTPUT);
    pinMode(horloge, OUTPUT);     
}

void loop()
{   
    long temps = 0;
  
    temps = millis();
 
    for(int i=0; i<17; i++)
    {
        unite = hexa[i];  
        while(unite <17)
        {
            unite ++;
        }
        seizaine = hexa[i];  
        while(seizaine <17)
        {
            seizaine ++;
        }
        digitalWrite(verrou, LOW);
        while((millis()-temps) < 8000)
        {
            shiftOut(donnee, horloge, LSBFIRST, seizaine);
        }
        while((millis()-temps) < 500)
        {
            shiftOut(donnee, horloge, LSBFIRST, unite);
        }
        digitalWrite(verrou, HIGH); 
       
    
        
     }
}
