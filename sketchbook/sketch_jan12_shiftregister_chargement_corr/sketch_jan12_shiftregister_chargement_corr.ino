//Broche connectée au ST_CP du 74HC595
const int verrou = 11;
//Broche connectée au SH_CP du 74HC595
const int horloge = 12;
//Broche connectée au DS du 74HC595
const int data = 10;
char sens = MSBFIRST;
//on commence à aller de droite vers gauche
char extinction = 0;
 
void setup()
{
    //On met les broches en sortie
    pinMode(verrou, OUTPUT);
    pinMode(horloge, OUTPUT);
    pinMode(data, OUTPUT);
}

 
void loop()
{
    //on démarre à 0 ou 1 selon...
    char donnee = extinction;
    for (int i = 0; i < 8; i++)
    {
        //On active le verrou le temps de transférer les données
        digitalWrite(verrou, LOW);
        //si on est en train d'éteindre
        if(extinction)
            //on envoie la donnée inversé
            shiftOut(data, horloge, MSBFIRST, ~donnee);
        else //sinon
            //on envoie la donnée normale
            shiftOut(data, horloge, LSBFIRST, donnee);
 
        //et enfin on relache le verrou
        digitalWrite(verrou, HIGH);
        //une petite pause pour constater l'affichage
        delay(250);
        //et on met à jour la donnée en cumulant les décalages
        donnee = donnee | (0x01 << i);
    }
    //permet d'inverser "MSBFIRST  LSBFIRST" comme dans l'exercice 2
    extinction = !extinction;
}
