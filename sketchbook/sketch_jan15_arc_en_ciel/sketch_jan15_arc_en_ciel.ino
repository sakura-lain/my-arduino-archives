const int ledRouge = 11;
const int ledVerte =  9;
const int ledBleue = 10;

void setup()
{
    // on déclare les broches en sorties
    pinMode(ledRouge, OUTPUT);
    pinMode(ledVerte, OUTPUT);
    pinMode(ledBleue, OUTPUT);
}

void loop()
{
    
    //rouge
    analogWrite(ledRouge, 255);
    analogWrite(ledVerte, 0);
    analogWrite(ledBleue, 0);
    delay(500);
    //orange
    analogWrite(ledRouge, 255);
    analogWrite(ledVerte, 127);
    analogWrite(ledBleue, 0);
    delay(500);
    //jaune
    analogWrite(ledRouge, 255);
    analogWrite(ledVerte, 255);
    analogWrite(ledBleue, 0);
    delay(500);
    //vert
    analogWrite(ledRouge, 0);
    analogWrite(ledVerte, 255);
    analogWrite(ledBleue, 0);
    delay(500);
    //blanc
    analogWrite(ledRouge, 255);
    analogWrite(ledVerte, 255);
    analogWrite(ledBleue, 255);
    delay(500);
    //bleu ciel
    analogWrite(ledRouge, 0);
    analogWrite(ledVerte, 255);
    analogWrite(ledBleue, 255);
    delay(500);
    //bleu
    analogWrite(ledRouge, 0);
    analogWrite(ledVerte, 0);
    analogWrite(ledBleue, 255);
    delay(500);
    //violet
    analogWrite(ledRouge, 255);
    analogWrite(ledVerte, 0);
    analogWrite(ledBleue, 255);
    delay(500);
    
}
