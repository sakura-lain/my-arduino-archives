const int ledRouge = 11;
const int ledVerte =  9;
const int ledBleue = 10;

void setup()
{
    // on déclare les broches en sorties
    pinMode(ledRouge, OUTPUT);
    pinMode(ledVerte, OUTPUT);
    pinMode(ledBleue, OUTPUT);
    
  
    analogWrite(ledRouge, 0);
    analogWrite(ledVerte, 0);
    analogWrite(ledBleue, 0);
}
void loop()
{

analogWrite(ledRouge, 255);

  for(int g=0; g<256; g++)
  {
    analogWrite(ledVerte, g);
    analogWrite(ledBleue, 0);
    delay(50);
  }
  for(int g=255; g>0; g--)
  {
    analogWrite(ledVerte, g);
    analogWrite(ledBleue, 0);
    delay(10);
  }
    for(int b=0; b<256; b++)
  {
    analogWrite(ledVerte, 0);
    analogWrite(ledBleue, b);
    delay(10);
  }

analogWrite(ledBleue, 255);

  for(int r=255; r>0; r--)
  {
    analogWrite(ledRouge, r);
    analogWrite(ledVerte, 0);
    delay(10);
  }
   for(int g=0; g<256; g++)
  {
    analogWrite(ledRouge, 0);
    analogWrite(ledVerte, g);
    delay(10);
  }
analogWrite(ledVerte, 255);
  for(int b=255; b>0; b--)
  {
    analogWrite(ledBleue, b);
    delay(10);
  }
   for(int r=0; r<256; r++)
  {
    analogWrite(ledRouge, r);
    delay(10);
  }
analogWrite(ledRouge, 255);

}

