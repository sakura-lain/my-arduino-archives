const int ledRouge = 11;
const int ledVerte =  9;
const int ledBleue = 10;

int brightness = 0;
int fadeAmount = 5; 

void setup()
{
    // on déclare les broches en sorties
    pinMode(ledRouge, OUTPUT);
    pinMode(ledVerte, OUTPUT);
    pinMode(ledBleue, OUTPUT);
    
}
void loop()
{
  // set the brightness of pin 9:
  analogWrite(ledRouge, brightness); 
  analogWrite(ledVerte, ~brightness);  
  analogWrite(ledBleue, brightness);
  delay(30);
  analogWrite(ledRouge, ~brightness); 
  analogWrite(ledVerte, brightness);  
  analogWrite(ledBleue, brightness);
  delay(30);
  
  // change the brightness for next time through the loop:
  brightness = brightness + fadeAmount;

  // reverse the direction of the fading at the ends of the fade: 
  if (brightness == 0 || brightness == 255) {
    fadeAmount = -fadeAmount ; 
  }     
  // wait for 30 milliseconds to see the dimming effect   

}
