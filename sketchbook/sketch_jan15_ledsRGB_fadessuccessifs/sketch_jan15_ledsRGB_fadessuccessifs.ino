const int ledRouge = 11;
const int ledVerte =  9;
const int ledBleue = 10;

void setup()
{
    // on déclare les broches en sorties
    pinMode(ledRouge, OUTPUT);
    pinMode(ledVerte, OUTPUT);
    pinMode(ledBleue, OUTPUT);

    // on met la valeur de chaque couleur
    analogWrite(ledRouge, 0);
    analogWrite(ledVerte, 0);
    analogWrite(ledBleue, 0);
}
void loop()
{
  
  for(int i=0; i<256; i++)
{
    analogWrite(ledRouge, i);
    analogWrite(ledVerte, 0);
    analogWrite(ledBleue, 0);
    delay(50);
}
for(int i=255; i>0; i--)
{
    analogWrite(ledRouge, i);
    analogWrite(ledVerte, 0);
    analogWrite(ledBleue, 0);
    delay(50);
}
for(int j=0; j<256; j++)
{
    analogWrite(ledRouge, 0);
    analogWrite(ledVerte, j);
    analogWrite(ledBleue, 0);
    delay(50);
}
for(int j=255; j>0; j--)
{
    analogWrite(ledRouge, 0);
    analogWrite(ledVerte, j);
    analogWrite(ledBleue, 0);
    delay(50);
}
for(int k=0; k<256; k++)
{
    analogWrite(ledRouge, 0);
    analogWrite(ledVerte, 0);
    analogWrite(ledBleue, k);
    delay(50);
}
for(int k=255; k>0; k--)
{
    analogWrite(ledRouge, 0);
    analogWrite(ledVerte, 0);
    analogWrite(ledBleue, k);
    delay(50);
}
}
