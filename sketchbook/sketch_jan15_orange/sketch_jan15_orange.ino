const int ledRouge = 11;
const int ledVerte =  9;
const int ledBleue = 10;

void setup()
{
    // on déclare les broches en sorties
    pinMode(ledRouge, OUTPUT);
    pinMode(ledVerte, OUTPUT);
    pinMode(ledBleue, OUTPUT);

    // on met la valeur de chaque couleur
    analogWrite(ledRouge, 255);
    analogWrite(ledVerte, 127);
    analogWrite(ledBleue, 0);
}

void loop()
{
    // on ne change pas la couleur donc rien à faire dans la boucle principale
}
