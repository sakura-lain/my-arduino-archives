#include <Servo.h>
Servo servo;
int pos = 0;
const int potar = A0;

void setup()
{
  servo.attach(2);
  pinMode(potar, INPUT);
  Serial.begin(9600);
}

void loop()
{
  int val = analogRead(potar);
  Serial.println(val);
  int angle = map(val, 0, 1023, 0, 179);
  servo.write(angle);
  Serial.println(angle);
  delay(15);
}

//monServo.write(map(analogRead(potar), 0, 1023, 0, 180));
