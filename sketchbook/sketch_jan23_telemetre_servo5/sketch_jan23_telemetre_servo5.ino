#define VITESSE 340

const int USTrig = 8; // Déclencheur sur la broche 8
const int USEcho = 9; // Réception sur la broche 9

#include <Servo.h> 
 
Servo servo;  // create servo object to control a servo 
                // a maximum of eight servo objects can be created 
 
int pos = 0;    // variable to store the servo position 

void setup() {
    pinMode(USTrig, OUTPUT);
    pinMode(USEcho, INPUT);
    servo.attach(6);  // attaches the servo on pin 6 to the servo object 

    digitalWrite(USTrig, LOW);

}

void loop()
{
   // 1. Un état haut de 10 microsecondes est mis sur la broche "Trig"
   digitalWrite(USTrig, HIGH);
   delayMicroseconds(10); //on attend 10 µs
   // 2. On remet à l’état bas la broche Trig
   digitalWrite(USTrig, LOW);
   // 3. On lit la durée d’état haut sur la broche "Echo"
   unsigned long duree = pulseIn(USEcho, HIGH);
   // 4. On divise cette durée par deux pour n'avoir qu'un trajet
   duree = duree/2;
   // 5. On calcule la distance avec la formule d=v*t
   float temps = duree/1000.0; //on met en secondes 10000 = cm ; 1000 = mm, etc.
   float distance = temps*VITESSE; //on multiplie par la vitesse, d=t*v

   if((distance <=100)&&(pos=0)){
       for(pos = 0; pos < 180; pos += 1)  // goes from 0 degrees to 180 degrees 
       {                                  // in steps of 1 degree 
            servo.write(180);              // tell servo to go to position in variable 'pos' 
            delay(15);                       // waits 15ms for the servo to reach the position 
       } 
   }
   else if((distance <=100)&&(pos =180)){
       for(pos = 180; pos>=1; pos-=1)     // goes from 180 degrees to 0 degrees 
       {                                
            servo.write(0);              // tell servo to go to position in variable 'pos' 
            delay(15);                       // waits 15ms for the servo to reach the position 
       } 
   }
   else{
     servo.write(180);
     delay(15);
   }
}

