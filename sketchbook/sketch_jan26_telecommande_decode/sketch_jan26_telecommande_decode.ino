#include <IRremote.h>
 
int pin_recept = 3; // On définit le pin 11  
IRrecv ir_recept(pin_recept); 
decode_results ir_decode; // stockage données reçues
 
void setup() 
{ 
  Serial.begin(9600); 
  ir_recept.enableIRIn(); // Initialisation de la réception 
}
 
void loop() 
{ 
  if (ir_recept.decode(&ir_decode)) 
  { 
    Serial.println(ir_decode.value, HEX); // On affiche le code en hexadecimal
    ir_recept.resume(); 
  } 
}

/*TOSHIBA :

Power : 0xA23D48B7

BOUTONS CENTRAUX :
Enter : 0xA23D22DD
Up : 0xA23D03FC
Down : 0xA23D13EC
Left : 0xA23D33CC
Right : 0xA23D23DC

BOUTONS PERIPHERIQUES HAUTS :
SlowL : 0xA23D31CE
SlowR : 0xA23D11EE
SkipL : 0xA23D21DE
SkipR : 0xA23D01FE

BOUTONS PERIPHERIQUES BAS :
Frame : 0xA23D7986
Adjust : 0xA23DB946
PictureL : 0xA23D19E6
PictureR : 0xA23D59A6

BOUTONS BLEUS :
Library : 0xA23DE21D
Rec : 0xA23D02FD
edit : 0xA23D827D
Content : 0xA23D42BD

BOUTONS BAS :
Pause : 0xA23DE817
Stop : 0xA23D6897
Play : 0xA23DC837
*/


