/*TOSHIBA :

Power : 0xA23D48B7

BOUTONS CENTRAUX :
Enter : 0xA23D22DD
Up : 0xA23D03FC
Down : 0xA23D13EC
Left : 0xA23D33CC
Right : 0xA23D23DC

BOUTONS PERIPHERIQUES HAUTS :
SlowL : 0xA23D31CE
SlowR : 0xA23D11EE
SkipL : 0xA23D21DE
SkipR : 0xA23D01FE

BOUTONS PERIPHERIQUES BAS :
Frame : 0xA23D7986
Adjust : 0xA23DB946
PictureL : 0xA23D19E6
PictureR : 0xA23D59A6
*/

#include <AFMotor.h>
#include <IRremote.h>

AF_DCMotor motor3(3, MOTOR34_1KHZ);
AF_DCMotor motor4(4, MOTOR34_1KHZ);
//create motor #2, 64KHz pwm
 
int pin_recept = 3; // On définit le pin 11  
IRrecv ir_recept(pin_recept); 
decode_results ir_decode; // stockage données reçues
int vitesse;


void setup(){
  
Serial.begin(9600);
// set up Serial library at 9600 bps
//motor3.setSpeed(255);
motor4.setSpeed(50);
// set the speed to 255

ir_recept.enableIRIn(); // Initialisation de la réception 
}

void loop()
{
  if (ir_recept.decode(&ir_decode)) 
  { 
    Serial.println(ir_decode.value, HEX); // On affiche le code en hexadecimal
    ir_recept.resume(); 
    //Marche avant :
   
    //Vitesse 255 :
    if (ir_decode.value == 0xA23D48B7){
      for(int v=50; v<255; v++){
      motor4.run(FORWARD); 
      motor4.setSpeed(v);
      delay(30);
      }
    }
    
  }
} 
