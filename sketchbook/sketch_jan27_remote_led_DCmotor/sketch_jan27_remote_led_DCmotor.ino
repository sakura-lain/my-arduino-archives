#include <AFMotor.h>
#include <IRremote.h>

AF_DCMotor motor(3, MOTOR34_1KHZ);
//create motor #2, 64KHz pwm
 
int pin_recept = 2; // On définit le pin 11  
IRrecv ir_recept(pin_recept); 
decode_results ir_decode; // stockage données reçues

const int led = 13;


void setup(){
  
Serial.begin(9600);
// set up Serial library at 9600 bps
motor.setSpeed(200);
// set the speed to 200/255
pinMode(led, OUTPUT);

ir_recept.enableIRIn(); // Initialisation de la réception 
}

void loop()
{
  if (ir_recept.decode(&ir_decode)) 
  { 
    Serial.println(ir_decode.value, HEX); // On affiche le code en hexadecimal
    ir_recept.resume(); 
    if (ir_decode.value == 0xA23D22DD){
      Serial.print("tick");
      motor.run(FORWARD);// turn it on going forward
      digitalWrite(led, HIGH);
    }
  }
} 
