/*TOSHIBA :

Power : 0xA23D48B7

BOUTONS CENTRAUX :
Enter : 0xA23D22DD
Up : 0xA23D03FC
Down : 0xA23D13EC
Left : 0xA23D33CC
Right : 0xA23D23DC

BOUTONS PERIPHERIQUES HAUTS :
SlowL : 0xA23D31CE
SlowR : 0xA23D11EE
SkipL : 0xA23D21DE
SkipR : 0xA23D01FE

BOUTONS PERIPHERIQUES BAS :
Frame : 0xA23D7986
Adjust : 0xA23DB946
PictureL : 0xA23D19E6
PictureR : 0xA23D59A6

BOUTONS BLEUS :
Library : 0xA23DE21D
Rec menu : 0xA23D02FD
Edit : 0xA23D827D
Content : 0xA23D42BD

BOUTONS BAS :
Pause : 0xA23DE817
Stop : 0xA23D6897
Play : 0xA23DC837
*/

/////////////// DECLARATIONS ///////////////

//Bibliothèques :
#include <AFMotor.h>
#include <IRremote.h>
#include <Servo.h>  

//DC Moteurs :
AF_DCMotor motor3(3, MOTOR34_1KHZ);//Crée le moteur #3 à 1KHz pwm
AF_DCMotor motor4(4, MOTOR34_1KHZ);//Crée le moteur #4 à 1KHz pwm
int vitesse;
 
//Télécommande :
int pin_recept = 3; // On définit le pin 3  
IRrecv ir_recept(pin_recept); 
decode_results ir_decode; // stockage des données reçues

//Servomoteur :
Servo servo; // Définit le nom servo pour el servomoteur
int servo_position = 0 ;

/////////////// SETUP ///////////////

void setup(){

//Initialisatiion de la communication série :  
Serial.begin(9600);
 
//Vitesse de départ des moteurs :
motor3.setSpeed(255);
motor4.setSpeed(255);

// Initialisation de la réception télécommande :
ir_recept.enableIRIn(); 

// Servo sur le pin 10 et orienté à 90° :
servo.attach (10);
servo.write(89);
}

/////////////// LOOP ///////////////

void loop()
{

//TELECOMMANDE~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~   

  if (ir_recept.decode(&ir_decode)) 
  { 
    Serial.println(ir_decode.value, HEX); // On affiche le code en hexadecimal
    ir_recept.resume(); 
    
//DIRECTIONS PRINCIPALES~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  

    //Marche avant :
    if (ir_decode.value == 0xA23D03FC)//Up
    {
      Serial.print("forward");
      motor3.run(FORWARD);
      motor4.run(FORWARD);
    }
    //Marche arrière :
    if (ir_decode.value == 0xA23D13EC)//Down
    {
      Serial.print("backward");
      motor3.run(BACKWARD);
      motor4.run(BACKWARD);
    }
    //Stop :
    if (ir_decode.value == 0xA23D22DD)//Enter
    {
      Serial.print("stop");
      motor3.run(RELEASE);
      motor4.run(RELEASE);
    }
    //Tourner à gauche :
    if (ir_decode.value == 0xA23D33CC)//Left
    {
      Serial.print("left");
      motor3.run(BACKWARD);
      motor4.run(FORWARD);
    }
    //Tourner à droite :
    if (ir_decode.value == 0xA23D23DC)//Right
    {
      Serial.print("right");
      motor3.run(FORWARD);
      motor4.run(BACKWARD);
    }
    
//TOURNER SUR PLACE~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~   

    //Tourner à gauche moteur bloqué - avant :
    if (ir_decode.value == 0xA23D31CE)//SlowL
    {
      Serial.print("left");
      motor3.run(RELEASE);
      motor4.run(FORWARD);
    }
    //Tourner à droite moteur bloqué - avant :
    if (ir_decode.value == 0xA23D11EE)//SkipR
    {
      Serial.print("right");
      motor3.run(FORWARD);
      motor4.run(RELEASE);
    }
    //Tourner à gauche moteur bloqué - arrière :
    if (ir_decode.value == 0xA23D21DE)//SlowR
    {
      Serial.print("left");
      motor3.run(BACKWARD);
      motor4.run(RELEASE);
    }
    //Tourner à droite moteur bloqué - arrière :
    if (ir_decode.value == 0xA23D01FE)//SkipL
    {
      Serial.print("right");
      motor3.run(RELEASE);
      motor4.run(BACKWARD);
    }
    
//VIRAGE~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    

    //Virage à gauche avant :
    if (ir_decode.value == 0xA23DB946)//Adjust
    {
      Serial.print("left");
      motor3.run(FORWARD);
      motor4.run(FORWARD);
      motor4.setSpeed(vitesse/2);
    }
    //Virage à droite avant :
    if (ir_decode.value == 0xA23D59A6)//PictureR
    {
      Serial.print("right");
      motor3.run(FORWARD);
      motor4.run(FORWARD);
      motor3.setSpeed(vitesse/2);
    }
    //Virage à gauche arrière :
    if (ir_decode.value == 0xA23D7986)//Frame
    {
      Serial.print("left");
      motor3.run(BACKWARD);
      motor4.run(BACKWARD);
      motor4.setSpeed(vitesse/2);
    }
    //Virage à droite arrière :
    if (ir_decode.value == 0xA23D19E6)//PictureL
    {
      Serial.print("right");
      motor3.run(BACKWARD);
      motor4.run(BACKWARD);
      motor3.setSpeed(vitesse/2);
    }
    
//VITESSE~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~     

    //Vitesse 100 :
    if (ir_decode.value == 0xA23DE21D)//Library
    {
      Serial.print("99");
      motor3.setSpeed(99);
      motor4.setSpeed(99);
      vitesse = 99;
    }
    //Vitesse 150 :
    if (ir_decode.value == 0xA23D02FD)//Rec menu
    {
      Serial.print("149");
      motor3.setSpeed(149);
      motor4.setSpeed(149);
      vitesse = 149;
    }
    //Vitesse 200 :
    if (ir_decode.value == 0xA23D827D)//Edit
    {
      Serial.print("199");
      motor3.setSpeed(199);
      motor4.setSpeed(199);
      vitesse = 199;
    }
    //Vitesse 255 :
    if (ir_decode.value == 0xA23D42BD)//Content
    {
      Serial.print("255");
      motor3.setSpeed(255);
      motor4.setSpeed(255);
      vitesse = 255;
    }
    
//SERVOMOTEUR~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

    //Servo 0 :
    if (ir_decode.value == 0xA23DE817)//Pause
    {
      Serial.print("servo 0");
      servo.write(0);
    }
    //Servo 90 :
    if (ir_decode.value == 0xA23D6897)//Stop
    {
      Serial.print("servo 90");
      servo.write(89);
    }
    //Servo 180 :
    if (ir_decode.value == 0xA23DC837)//Play
    {
      Serial.print("servo 180");
      servo.write(179); 
    }
    
//TELECOMMANDE
  }
  
//LOOP
} 
